var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');

var player;
var keyboard;

var player1;
var player2;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;

var distance = 0;
var status = 'running';

var menuState = {
    preload: function(){
        preload();
    },
    create: function(){
        game.add.image(0, 0, 'background');
        game.BackGroundMusic = game.add.audio('bgm');
        game.BackGroundMusic.loop = true;
        game.BackGroundMusic.volume = 0.5;
        game.BackGroundMusic.play();
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        var style = {fill: '#ff0000', fontSize: '20px'}
        text2 = game.add.text(75, 130, 'PRESS ↓ TO START SINGLE', style);
        text3 = game.add.text(75, 150, 'PRESS S TO START MULTI', style);
    },
    update: function(){
        if(keyboard.down.isDown){
            game.state.start('1P');
            status = 'running';
            distance = 0;
        }
        else if(keyboard.s.isDown){
            game.state.start('2P');
            status = 'running';
            distance = 0;
        }
    }
};

var singleState = {
    preload: function(){
        preload();
    },
    create: function(){
        create();
    },
    update: function(){
        update();
    }
};

var multiState = {
    preload: function(){
        preload();
    },
    create: function(){
        create1();
    },
    update: function(){
        update1();
    }
};

function preload () {
    game.load.image('background', 'assets/background.png');
    game.load.spritesheet('player', './assets/player.png', 32, 32);
    game.load.spritesheet('player2', './assets/player2.png', 32, 32);
    game.load.image('normal', './assets/normal.png');
    game.load.image('nails', './assets/nails.png');
    game.load.spritesheet('conveyorRight', './assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', './assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', './assets/fake.png', 96, 36);
    game.load.image('wall', './assets/wall.png');
    game.load.image('ceiling', './assets/ceiling.png');
    game.load.audio('bgm', 'assets/sounds/bgm.ogg');
    game.load.audio('dead', 'assets/sounds/dead.ogg');
    game.load.audio('hit', 'assets/sounds/hit.wav');
    game.load.audio('jump', 'assets/sounds/jump.ogg');
}

function create () {
    game.add.image(0, 0, 'background');
    keyboard = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D
    });
    createBounders();
    createPlayer();
    createTextsBoard();
}

function update () {
    // bad
    if(status == 'gameOver' && keyboard.enter.isDown) game.state.start('menu');
    if(status != 'running') return;
    game.physics.arcade.collide(player, platforms, effect);
    game.physics.arcade.collide(player, [leftWall, rightWall]);
    checkTouchCeiling(player);
    checkGameOver();

    updatePlayer();
    updatePlatforms();
    updateTextsBoard();

    createPlatforms();
}

function create1 () {
    game.add.image(0, 0, 'background');
    keyboard = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D
    });
    createBounders();
    createPlayer1();
    createTextsBoard1();
}

function update1 () {
    // bad
    if(status == 'gameOver' && keyboard.enter.isDown) game.state.start('menu');
    if(status != 'running') return;
    game.physics.arcade.collide([player1, player2], platforms, effect);
    game.physics.arcade.collide([player1, player2], [leftWall, rightWall]);
    game.physics.arcade.collide(player1, player2);
    checkTouchCeiling(player1);
    checkTouchCeiling(player2);
    checkGameOver1();

    updatePlayer1();
    updatePlatforms();
    updateTextsBoard1();

    createPlatforms();
}

function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms () {
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 18);
    player.animations.add('right', [9, 10, 11, 12], 18);
    player.animations.add('flyleft', [18, 19, 20, 21], 18);
    player.animations.add('flyright', [27, 28, 29, 30], 18);
    player.animations.add('fly', [36, 37, 38, 39], 18);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createPlayer1 () {
    player1 = game.add.sprite(300, 50, 'player');
    player2 = game.add.sprite(100, 50, 'player2');

    setPlayerAttr(player1);
    setPlayerAttr(player2);
}

function setPlayerAttr(player) {
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(350, 10, '', style);
    text3 = game.add.text(75, 150, 'PRESS ENTER TO RETURN', style);
    text3.visible = false;
}

function createTextsBoard1 () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(350, 10, '', style);
    text3 = game.add.text(75, 130, '', style);
    text3.visible = false;
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function updatePlayer1 () {
    if(keyboard.left.isDown) {
        player1.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player1.body.velocity.x = 250;
    } else {
        player1.body.velocity.x = 0;
    }

    if(keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if(keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate(player1);
    setPlayerAnimate(player2);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}

function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life);
    text2.setText('B' + distance);
}

function updateTextsBoard1 () {
    text1.setText('life:' + player1.life);
    text2.setText('life:' + player2.life);
}

function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    if(player.life < 10){
        player.life += 1;
    }
    platform.animations.play('jump');
    player.body.velocity.y = -280;
    this.jumpSound = game.add.audio('jump');
    this.jumpSound.play();
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 4;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
        this.hitSound = game.add.audio('hit');
        this.hitSound.volume = 2.5;
        this.hitSound.play();
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        if(player.life < 10){
            player.life += 1;
        }
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            this.hitSound = game.add.audio('hit');
            this.hitSound.volume = 2.5;
            this.hitSound.play();
            player.life -= 4;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver () {
    if(player.life <= 0 || player.body.y > 500) {
        gameOver();
    }
}

function checkGameOver1 () {
    if(player1.life <= 0 || player1.body.y > 500) {
        gameOver1('player2');
    }
    if(player2.life <= 0 || player2.body.y > 500) {
        gameOver1('player1');
    }
}

function gameOver () {
    this.deadSound = game.add.audio('dead');
    this.deadSound.play();
    game.BackGroundMusic.stop();
    text3.visible = true;
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    status = 'gameOver';
}

function gameOver1(winner) {
    this.deadSound = game.add.audio('dead');
    this.deadSound.play();
    game.BackGroundMusic.stop();
    text3.visible = true;
    text3.setText(winner + '    wins!' + '\nPRESS ENTER TO RETURN');
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    status = 'gameOver';
}

game.state.add('menu', menuState);
game.state.add('1P', singleState);
game.state.add('2P', multiState);
game.state.start('menu');