# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


* A complete game process: start menu => game view => game over => quit or play again
    * 有主選單->遊戲->死亡->主選單->...。

* Your game should follow the basic rules of  "小朋友下樓梯".
    * 有遵循小朋友下樓梯的規則，尖刺扣血，踩到地板回血(彈簧彈幾次回幾滴)。

* All things in your game should have correct physical properties and behaviors.
    * 有使用下降、跳躍、飛行等物理引擎。

* Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform)
    * 除了普通地板外，有尖刺、彈簧、假地板、輸送帶。

* Add some additional sound effects and UI to enrich your game.
    * 在踩到尖刺、彈簧，還有死掉時有特殊音效，在關卡中也有背景音樂。

* Appearance (subjective)
    * 遊戲除了左右牆壁、上面尖刺、以及各種地板外，還有背景增加美觀。

* Other creative features in your game
    * 單人模式時右上角有紀錄地下幾層
    * 雙人模式：有一方掉下去時分出勝負